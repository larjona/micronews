Title: This weekend is MiniDebConf Cambridge 2017! Get the info about schedule and live streaming at https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge
Slug: 1511624193
Date: 2017-11-25 15:36
Author: Laura Arjona Reina
Status: published

This weekend is MiniDebConf Cambridge 2017! Get the info about schedule and live streaming at [https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge](https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge)
