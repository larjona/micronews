Title: Debian Project News - 09 Nov 2017: Debian 9.2 released, OpenSSL 1.0 and Qt4 removal in Buster, FTP services shutdown, DebConf mailing lists moved, MiniDebConf in Cuba, and much more https://www.debian.org/News/weekly/2017/04/
Slug: 1510317585
Date: 2017-11-10 12:39
Author: Laura Arjona Reina
Status: published

Debian Project News - 09 Nov 2017: Debian 9.2 released, OpenSSL 1.0 and Qt4 removal in Buster, FTP services shutdown, DebConf mailing lists moved, MiniDebConf in Cuba, and much more [https://www.debian.org/News/weekly/2017/04/](https://www.debian.org/News/weekly/2017/04/)
