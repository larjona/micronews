Title: Debsources collects per-suite stats, computing various metrics. See the stats for Stretch: http://sources.debian.net/stats/stretch/ #releasingstretch
Slug: 1497709566
Date: 2017-06-17 14:26
Author: Laura Arjona Reina
Status: published

Debsources collects per-suite stats, computing various metrics. See the stats for Stretch: [http://sources.debian.net/stats/stretch/](http://sources.debian.net/stats/stretch/) #releasingstretch
