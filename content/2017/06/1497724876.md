Title: Stretch has 25,357 source packages with 9,808,465 source files. See more stats on debsources http://sources.debian.net/stats/stretch/ #releasingstretch
Slug: 1497724876
Date: 2017-06-17 18:41
Author: Paul Wise
Status: published

Stretch has 25,357 source packages with 9,808,465 source files. See more stats on debsources [http://sources.debian.net/stats/stretch/](http://sources.debian.net/stats/stretch/) #releasingstretch
