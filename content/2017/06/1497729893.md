Title: Debian Popularity Contest gathers anonymous statistics about Debian packages usage from about 195,000 reports http://popcon.debian.org/ #releasingstretch
Slug: 1497729893
Date: 2017-06-17 20:04
Author: Cédric Boutillier
Status: published

Debian Popularity Contest gathers anonymous statistics about Debian packages usage from about 195,000 reports [http://popcon.debian.org/](http://popcon.debian.org/) #releasingstretch
