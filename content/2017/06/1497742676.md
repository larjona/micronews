Title: There are at least 95 different Debian derivatives known to the derivatives census https://wiki.debian.org/Derivatives/Census #releasingstretch
Slug: 1497742676
Date: 2017-06-17 23:37
Author: Cédric Boutillier
Status: published

There are at least 95 different Debian derivatives known to the derivatives census [https://wiki.debian.org/Derivatives/Census](https://wiki.debian.org/Derivatives/Census) #releasingstretch
