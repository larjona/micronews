Title: There are several ways to donate to Debian, the auditors are working on adding more https://www.debian.org/donations
Slug: 1497727930
Date: 2017-06-17 19:32
Author: Cédric Boutillier
Status: published

There are several ways to donate to Debian, the auditors are working on adding more [https://www.debian.org/donations](https://www.debian.org/donations)
