Title: The Debian bug tracker has added the a11y tag for tracking accessibility issues https://www.debian.org/Bugs/Developer#tags
Slug: 1498664332
Date: 2017-06-28 15:38
Author: Laura Arjona Reina
Status: published

The Debian bug tracker has added the a11y tag for tracking accessibility issues [https://www.debian.org/Bugs/Developer#tags](https://www.debian.org/Bugs/Developer#tags)
