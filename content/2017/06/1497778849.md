Title: Please be aware of the issues that testers found with Debian stretch install media https://wiki.debian.org/Teams/DebianCD/ReleaseTesting/Stretch#Errata #releasingstretch
Slug: 1497778849
Date: 2017-06-18 09:40
Author: Paul Wise
Status: published

Please be aware of the issues that testers found with Debian stretch install media [https://wiki.debian.org/Teams/DebianCD/ReleaseTesting/Stretch#Errata](https://wiki.debian.org/Teams/DebianCD/ReleaseTesting/Stretch#Errata) #releasingstretch
