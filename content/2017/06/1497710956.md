Title: The Debian Stretch desktop theme is "softWaves" by Juliette Taka Belin https://wiki.debian.org/DebianArt/Themes/softWaves #releasingstretch
Slug: 1497710956
Date: 2017-06-17 14:49
Author: Paul Wise
Status: published

The Debian Stretch desktop theme is "softWaves" by Juliette Taka Belin [https://wiki.debian.org/DebianArt/Themes/softWaves](https://wiki.debian.org/DebianArt/Themes/softWaves) #releasingstretch
