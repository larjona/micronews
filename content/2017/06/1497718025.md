Title: Following Pixar's Toy Story naming scheme, Debian's releases have been: Buzz, Rex, Bo, Hamm, Slink, Potato, Woody, Sarge, Etch, Lenny, Squeeze, Wheezy, Jessie and now Stretch #releasingstretch
Slug: 1497718025
Date: 2017-06-17 16:47
Author: Paul Wise
Status: published

Following Pixar's Toy Story naming scheme, Debian's releases have been: Buzz, Rex, Bo, Hamm, Slink, Potato, Woody, Sarge, Etch, Lenny, Squeeze, Wheezy, Jessie and now Stretch #releasingstretch
