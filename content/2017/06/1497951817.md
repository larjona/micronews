Title: There are important security updates for glibc/linux/exim4. Update your systems! Security updates are sent to debian-security-announce mailing list https://lists.debian.org/debian-security-announce/
Slug: 1497951817
Date: 2017-06-20 09:43
Author: Ana Guerrero Lopez
Status: published

There are important security updates for glibc/linux/exim4. Update your systems! Security updates are sent to debian-security-announce mailing list [https://lists.debian.org/debian-security-announce/](https://lists.debian.org/debian-security-announce/)
