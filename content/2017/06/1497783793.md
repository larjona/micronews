Title: Debian would like to thank our 19 partner organisations, who provide hardware, employ developers, donate money, hosting and more https://www.debian.org/partners/ #releasingstretch
Slug: 1497783793
Date: 2017-06-18 11:03
Author: Paul Wise
Status: published

Debian would like to thank our 19 partner organisations, who provide hardware, employ developers, donate money, hosting and more [https://www.debian.org/partners/](https://www.debian.org/partners/) #releasingstretch
