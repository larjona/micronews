Title: Debian would like to thank our 6 trusted organisations, who accept donations for and hold assets in trust for Debian. https://wiki.debian.org/Teams/Auditor/Organizations #releasingstretch
Slug: 1497787211
Date: 2017-06-18 12:00
Author: Paul Wise
Status: published

Debian would like to thank our 6 trusted organisations, who accept donations for and hold assets in trust for Debian. [https://wiki.debian.org/Teams/Auditor/Organizations](https://wiki.debian.org/Teams/Auditor/Organizations) #releasingstretch
