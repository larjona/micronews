Title: Afternoon talk at DebConf17: "When Social Issues Do Not Matter In Technical Debates (and when they do)" by Katheryn Sutter https://debconf17.debconf.org/talks/134/
Slug: 1502487031
Date: 2017-08-11 21:30
Author: Laura Arjona Reina
Status: published

Afternoon talk at DebConf17: "When Social Issues Do Not Matter In Technical Debates (and when they do)" by Katheryn Sutter [https://debconf17.debconf.org/talks/134/](https://debconf17.debconf.org/talks/134/)
