Title: Next August 16th is Debian Day 2017. Organize or attend a party! https://wiki.debian.org/DebianDay/2017
Slug: 1502615779
Date: 2017-08-13 09:16
Author: Laura Arjona Reina
Status: published

Next August 16th is Debian Day 2017. Organize or attend a party! [https://wiki.debian.org/DebianDay/2017](https://wiki.debian.org/DebianDay/2017)
