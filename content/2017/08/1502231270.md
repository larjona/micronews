Title: Next in DebConf17: Package build dependencies, Installing Debian, Debian in the modern pipeline https://debconf17.debconf.org/schedule/?day=2017-08-08
Slug: 1502231270
Date: 2017-08-08 22:27
Author: Laura Arjona Reina
Status: published

Next in DebConf17: Package build dependencies, Installing Debian, Debian in the modern pipeline [https://debconf17.debconf.org/schedule/?day=2017-08-08](https://debconf17.debconf.org/schedule/?day=2017-08-08)
