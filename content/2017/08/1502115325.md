Title: DebConf17 talks for this morning: about Flatpak, Git for packaging, 32-bit time_t ... https://debconf17.debconf.org/schedule/?day=2017-08-07
Slug: 1502115325
Date: 2017-08-07 14:15
Author: Laura Arjona Reina
Status: published

DebConf17 talks for this morning: about Flatpak, Git for packaging, 32-bit time_t ... [https://debconf17.debconf.org/schedule/?day=2017-08-07](https://debconf17.debconf.org/schedule/?day=2017-08-07)
