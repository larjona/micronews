Title: Debian thanks the commitment of numerous sponsors to support DebConf17 https://debconf17.debconf.org/sponsors/
Slug: 1502585161
Date: 2017-08-13 00:46
Author: Laura Arjona Reina
Status: published

Debian thanks the commitment of numerous sponsors to support DebConf17 [https://debconf17.debconf.org/sponsors/](https://debconf17.debconf.org/sponsors/)
