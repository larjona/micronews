Title: Morning talks today at DebConf17: Am I really married with Debian? and Using autopkgtest results for unstable to testing migration https://debconf17.debconf.org/schedule/?day=2017-08-11
Slug: 1502457821
Date: 2017-08-11 13:23
Author: Laura Arjona Reina
Status: published

Morning talks today at DebConf17: Am I really married with Debian? and Using autopkgtest results for unstable to testing migration [https://debconf17.debconf.org/schedule/?day=2017-08-11](https://debconf17.debconf.org/schedule/?day=2017-08-11)
