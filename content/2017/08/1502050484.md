Title: This afternoon, talks at DebConf17 about the Debian Publicity team, motivations to keep contributing to Debian, and Debian Blends related to scientific software https://debconf17.debconf.org/schedule/
Slug: 1502050484
Date: 2017-08-06 20:14
Author: Laura Arjona Reina
Status: published

This afternoon, talks at DebConf17 about the Debian Publicity team, motivations to keep contributing to Debian, and Debian Blends related to scientific software [https://debconf17.debconf.org/schedule/](https://debconf17.debconf.org/schedule/)
