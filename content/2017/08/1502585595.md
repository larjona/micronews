Title: DebConf17 closes in Montreal and DebConf18 dates announced https://bits.debian.org/2017/08/debconf17-closes.html
Slug: 1502585595
Date: 2017-08-13 00:53
Author: Laura Arjona Reina
Status: published

DebConf17 closes in Montreal and DebConf18 dates announced [https://bits.debian.org/2017/08/debconf17-closes.html](https://bits.debian.org/2017/08/debconf17-closes.html)
