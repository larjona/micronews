Title: "All ages" and SPI BOF continue at DebConf17, starting Let's use Ed25519 with GnuPG 2.1 and Gnuk Token https://debconf17.debconf.org/schedule/?day=2017-08-10
Slug: 1502411978
Date: 2017-08-11 00:39
Author: Laura Arjona Reina
Status: published

"All ages" and SPI BOF continue at DebConf17, starting Let's use Ed25519 with GnuPG 2.1 and Gnuk Token [https://debconf17.debconf.org/schedule/?day=2017-08-10](https://debconf17.debconf.org/schedule/?day=2017-08-10)
