Title: No live streaming today, but the first DebConf17 videos are available! http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/
Slug: 1502306144
Date: 2017-08-09 19:15
Author: Laura Arjona Reina
Status: published

No live streaming today, but the first DebConf17 videos are available! [http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/](http://meetings-archive.debian.net/pub/debian-meetings/2017/debconf17/)
