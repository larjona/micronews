Title: Debian turns 24! https://bits.debian.org/2017/08/debian-turns-24.html
Slug: 1502905921
Date: 2017-08-16 17:52
Author: Laura Arjona Reina
Status: published

Debian turns 24! [https://bits.debian.org/2017/08/debian-turns-24.html](https://bits.debian.org/2017/08/debian-turns-24.html)
