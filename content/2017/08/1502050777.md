Title: A job fair with some of our sponsors and a workshop about packaging for beginners will also take place today at DebConf17 https://debconf17.debconf.org/sponsors/
Slug: 1502050777
Date: 2017-08-06 20:19
Author: Laura Arjona Reina
Status: published

A job fair with some of our sponsors and a workshop about packaging for beginners will also take place today at DebConf17 [https://debconf17.debconf.org/sponsors/](https://debconf17.debconf.org/sponsors/)
