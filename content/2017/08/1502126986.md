Title: OpenSSL disables TLS 1.0 and 1.1 (in unstable) https://lists.debian.org/debian-devel-announce/2017/08/msg00004.html
Slug: 1502126986
Date: 2017-08-07 17:29
Author: Laura Arjona Reina
Status: published

OpenSSL disables TLS 1.0 and 1.1 (in unstable) [https://lists.debian.org/debian-devel-announce/2017/08/msg00004.html](https://lists.debian.org/debian-devel-announce/2017/08/msg00004.html)
