Title: Closing ceremony of DebConf17. Follow the streaming! https://debconf17.debconf.org/talks/185/
Slug: 1502584656
Date: 2017-08-13 00:37
Author: Laura Arjona Reina
Status: published

Closing ceremony of DebConf17. Follow the streaming! [https://debconf17.debconf.org/talks/185/](https://debconf17.debconf.org/talks/185/)
