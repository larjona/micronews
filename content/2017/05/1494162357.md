Title: "New in Debian stable Stretch: nftables" by Arturo Borrero González http://ral-arturo.org/2017/05/05/debian-stretch-stable-nftables.html
Slug: 1494162357
Date: 2017-05-07 13:05
Author: Laura Arjona Reina
Status: published

"New in Debian stable Stretch: nftables" by Arturo Borrero González [http://ral-arturo.org/2017/05/05/debian-stretch-stable-nftables.html](http://ral-arturo.org/2017/05/05/debian-stretch-stable-nftables.html)
