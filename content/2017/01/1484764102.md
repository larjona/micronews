Title: Debian hexagonal stickers made by Elena Grandi, available in the website https://www.debian.org/logos/
Slug: 1484764102
Date: 2017-01-18 18:28
Author: Laura Arjona Reina
Status: published

Debian hexagonal stickers made by Elena Grandi, available in the website [https://www.debian.org/logos/](https://www.debian.org/logos/)
