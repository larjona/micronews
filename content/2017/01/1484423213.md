Title: Updated Debian 8: 8.7 released http://www.debian.org/News/2017/20170114
Slug: 1484423213
Date: 2017-01-14 19:46:53
Author: Cédric Boutillier
Status: published

Updated Debian 8: 8.7 released [http://www.debian.org/News/2017/20170114](http://www.debian.org/News/2017/20170114)
