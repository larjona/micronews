Title: Debian Installer Stretch RC 1 release https://lists.debian.org/debian-devel-announce/2017/01/msg00004.html
Slug: 1484562906
Date: 2017-01-16 10:35
Author: Laura Arjona Reina
Status: published

Debian Installer Stretch RC 1 release [https://lists.debian.org/debian-devel-announce/2017/01/msg00004.html](https://lists.debian.org/debian-devel-announce/2017/01/msg00004.html)
