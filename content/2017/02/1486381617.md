Title: Bits from the Release Team: stretch is frozen https://lists.debian.org/debian-devel-announce/2017/02/msg00001.html
Slug: 1486381617
Date: 2017-02-06 11:46
Author: Laura Arjona Reina
Status: published

Bits from the Release Team: stretch is frozen [https://lists.debian.org/debian-devel-announce/2017/02/msg00001.html](https://lists.debian.org/debian-devel-announce/2017/02/msg00001.html)
