Title: Call for projects and mentors for the June-August round of Debian internships https://lists.debian.org/debian-devel-announce/2017/02/msg00003.html
Slug: 1486930410
Date: 2017-02-12 20:13
Author: Laura Arjona Reina
Status: published

Call for projects and mentors for the June-August round of Debian internships [https://lists.debian.org/debian-devel-announce/2017/02/msg00003.html](https://lists.debian.org/debian-devel-announce/2017/02/msg00003.html)
