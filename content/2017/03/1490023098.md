Title: DebConf17 welcomes its first eighteen sponsors! https://bits.debian.org/2017/03/dc17-welcome-its-first-sponsors.html
Slug: 1490023098
Date: 2017-03-20 15:18
Author: Laura Arjona Reina
Status: published

DebConf17 welcomes its first eighteen sponsors! [https://bits.debian.org/2017/03/dc17-welcome-its-first-sponsors.html](https://bits.debian.org/2017/03/dc17-welcome-its-first-sponsors.html)
