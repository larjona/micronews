Title: Videos of the recent MiniDebConf Cambridge are available, thanks DebConf Video Team! See details and links at https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge
Slug: 1512400934
Date: 2017-12-04 15:22
Author: Laura Arjona Reina
Status: published

Videos of the recent MiniDebConf Cambridge are available, thanks DebConf Video Team! See details and links at [https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge](https://wiki.debian.org/DebianEvents/gb/2017/MiniDebConfCambridge)
