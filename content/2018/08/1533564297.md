Title: If you want Debian Buster released in early 2019 (or like short Debian freezes in general...) help fix RC bugs today! https://nthykier.wordpress.com/2018/08/06/buster-is-headed-for-a-long-hard-freeze/
Slug: 1533564297
Date: 2018-08-06 14:04
Author: Laura Arjona Reina
Status: published

If you want Debian Buster released in early 2019 (or like short Debian freezes in general...) help fix RC bugs today! [https://nthykier.wordpress.com/2018/08/06/buster-is-headed-for-a-long-hard-freeze/](https://nthykier.wordpress.com/2018/08/06/buster-is-headed-for-a-long-hard-freeze/)
