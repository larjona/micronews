Title: The original announcement from Ian Murdock can be found here: https://deb.li/ikd3y #DebianDay #Debian25years
Slug: 1534435846
Date: 2018-08-16 16:10
Author: Martin Zobel-Helas
Status: published

The original announcement from Ian Murdock can be found here: [https://deb.li/ikd3y](https://deb.li/ikd3y) #DebianDay #Debian25years
