Title: This morning starts with "Running tests and analysing results on an Embedded Debian system with BMW/Genivi tooling" https://debconf18.debconf.org/talks/17-running-tests-and-analysing-results-on-an-embedded-debian-system-with-bmwgenivi-tooling/
Slug: 1533268814
Date: 2018-08-03 04:00
Author: Laura Arjona Reina
Status: published

This morning starts with "Running tests and analysing results on an Embedded Debian system with BMW/Genivi tooling" [https://debconf18.debconf.org/talks/17-running-tests-and-analysing-results-on-an-embedded-debian-system-with-bmwgenivi-tooling/](https://debconf18.debconf.org/talks/17-running-tests-and-analysing-results-on-an-embedded-debian-system-with-bmwgenivi-tooling/)
