Title: This afternoon at #DebConf18 continues with several ad-hoc events, some of them with video coverage https://debconf18.debconf.org/schedule/?day=2018-08-04
Slug: 1533370842
Date: 2018-08-04 08:20
Author: Laura Arjona Reina
Status: published

This afternoon at #DebConf18 continues with several ad-hoc events, some of them with video coverage [https://debconf18.debconf.org/schedule/?day=2018-08-04](https://debconf18.debconf.org/schedule/?day=2018-08-04)
