Title: The morning starts with "FAI.me - A Build Service for Installation and Cloud Images". Follow the live streaming at https://debconf18.debconf.org
Slug: 1533181759
Date: 2018-08-02 03:49
Author: Laura Arjona Reina
Status: published

The morning starts with "FAI.me - A Build Service for Installation and Cloud Images". Follow the live streaming at [https://debconf18.debconf.org](https://debconf18.debconf.org)
