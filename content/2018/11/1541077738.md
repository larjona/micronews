Title: Perl 5.28 transition underway, wide uninstallability is to be expected in sid for the next days! https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html
Slug: 1541077738
Date: 2018-11-01 13:08
Author: Ana Guerrero Lopez
Status: published

Perl 5.28 transition underway, wide uninstallability is to be expected in sid for the next days! [https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html](https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html)
