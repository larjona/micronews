Title: Registration is now open for DebConf18, in Hsinchu, Taiwan https://debconf18.debconf.org/news/2018-03-16-registration/
Slug: 1521212184
Date: 2018-03-16 14:56
Author: Laura Arjona Reina
Status: published

Registration is now open for DebConf18, in Hsinchu, Taiwan [https://debconf18.debconf.org/news/2018-03-16-registration/](https://debconf18.debconf.org/news/2018-03-16-registration/)
