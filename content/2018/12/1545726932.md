Title: We wish happy holidays to all Debian contributors, Open Source enthusiasts and to the rest of the world. Have a few relaxing days.
Slug: 1545726932
Date: 2018-12-25 08:35
Author: Martin Zobel-Helas
Status: published

We wish happy holidays to all Debian contributors, Open Source enthusiasts and to the rest of the world. Have a few relaxing days.
