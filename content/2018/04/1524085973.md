Title: "Re-elected as Debian Project Leader", by Chris Lamb https://chris-lamb.co.uk/posts/re-elected-as-debian-project-leader
Slug: 1524085973
Date: 2018-04-18 21:12
Author: Laura Arjona Reina
Status: published

"Re-elected as Debian Project Leader", by Chris Lamb [https://chris-lamb.co.uk/posts/re-elected-as-debian-project-leader](https://chris-lamb.co.uk/posts/re-elected-as-debian-project-leader)
