Title: DebConf18 Call for Proposals is still open! Submit your talk up to June 17th 2018 to be considered. https://debconf18.debconf.org/cfp/
Slug: 1524090857
Date: 2018-04-18 22:34
Author: Martin Zobel-Helas
Status: published

DebConf18 Call for Proposals is still open! Submit your talk up to June 17th 2018 to be considered. [https://debconf18.debconf.org/cfp/](https://debconf18.debconf.org/cfp/)
