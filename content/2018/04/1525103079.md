Title: Bits from the Debian Project Leader (April 2018) -- https://lists.debian.org/debian-devel-announce/2018/04/msg00012.html
Slug: 1525103079
Date: 2018-04-30 15:44
Author: Chris Lamb
Status: published

Bits from the Debian Project Leader (April 2018) -- [https://lists.debian.org/debian-devel-announce/2018/04/msg00012.html](https://lists.debian.org/debian-devel-announce/2018/04/msg00012.html)
