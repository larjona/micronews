Title: Debian 12 (after "buster" and "bullseye") will be called "bookworm" -- https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html
Slug: 1523981775
Date: 2018-04-17 16:16
Author: Chris Lamb
Status: published

Debian 12 (after "buster" and "bullseye") will be called "bookworm" -- [https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html](https://lists.debian.org/debian-devel-announce/2018/04/msg00006.html)
