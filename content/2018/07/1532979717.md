Title: New Debian Developers and Maintainers (May and June 2018) https://bits.debian.org/2018/07/new-developers-2018-06.html
Slug: 1532979717
Date: 2018-07-30 19:41
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (May and June 2018) [https://bits.debian.org/2018/07/new-developers-2018-06.html](https://bits.debian.org/2018/07/new-developers-2018-06.html)
