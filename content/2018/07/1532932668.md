Title:  #DebConf18 is committed to a safe environment for all participants. See our code of conduct: https://debconf.org/codeofconduct.shtml
Slug: 1532932668
Date: 2018-07-30 06:37
Author: Laura Arjona Reina
Status: published

 #DebConf18 is committed to a safe environment for all participants. See our code of conduct: [https://debconf.org/codeofconduct.shtml](https://debconf.org/codeofconduct.shtml)
