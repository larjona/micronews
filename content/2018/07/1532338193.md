Title: The Debian Policy Sprint is ongoing at DebCamp! check https://wiki.debian.org/Sprints/2018/DebianPolicy #DebConf18
Slug: 1532338193
Date: 2018-07-23 09:29
Author: Martin Zobel-Helas
Status: published

The Debian Policy Sprint is ongoing at DebCamp! check [https://wiki.debian.org/Sprints/2018/DebianPolicy](https://wiki.debian.org/Sprints/2018/DebianPolicy) #DebConf18
