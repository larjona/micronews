Title: This evening #DebConf18 celebrates its traditional "Cheese & Wine Party". Learn more about this social event in https://wiki.debconf.org/wiki/DebConf18/CheeseWineBoF
Slug: 1532952020
Date: 2018-07-30 12:00
Author: Laura Arjona Reina
Status: published

This evening #DebConf18 celebrates its traditional "Cheese & Wine Party". Learn more about this social event in [https://wiki.debconf.org/wiki/DebConf18/CheeseWineBoF](https://wiki.debconf.org/wiki/DebConf18/CheeseWineBoF)
