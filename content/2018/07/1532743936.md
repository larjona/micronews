Title: Today is the #DebConf18 Open Day! See the schedule and follow the live streaming https://debconf18.debconf.org/schedule/open-day/
Slug: 1532743936
Date: 2018-07-28 02:12
Author: Laura Arjona Reina
Status: published

Today is the #DebConf18 Open Day! See the schedule and follow the live streaming [https://debconf18.debconf.org/schedule/open-day/](https://debconf18.debconf.org/schedule/open-day/)
