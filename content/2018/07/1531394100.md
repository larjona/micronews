Title: Debian joins KDE's Advisory Board -- https://dot.kde.org/2018/07/12/debian-joins-kdes-advisory-board
Slug: 1531394100
Date: 2018-07-12 11:15
Author: Chris Lamb
Status: published

Debian joins KDE's Advisory Board -- [https://dot.kde.org/2018/07/12/debian-joins-kdes-advisory-board](https://dot.kde.org/2018/07/12/debian-joins-kdes-advisory-board)
