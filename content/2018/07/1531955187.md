Title: The countdown begins, DebCamp will start in three days! https://wiki.debconf.org/wiki/DebConf18/DebCamp #DebConf18
Slug: 1531955187
Date: 2018-07-18 23:06
Author: Martin Zobel-Helas
Status: published

The countdown begins, DebCamp will start in three days! [https://wiki.debconf.org/wiki/DebConf18/DebCamp](https://wiki.debconf.org/wiki/DebConf18/DebCamp) #DebConf18
