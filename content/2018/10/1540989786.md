Title: Bits from the Debian Project Leader (October 2018) -- https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html
Slug: 1540989786
Date: 2018-10-31 12:43
Author: Chris Lamb
Status: published

Bits from the Debian Project Leader (October 2018) -- [https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html](https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html)
