Title: New Debian Developers and Maintainers (March and April 2018) https://bits.debian.org/2018/05/new-developers-2018-04.html
Slug: 1525298605
Date: 2018-05-02 22:03
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (March and April 2018) [https://bits.debian.org/2018/05/new-developers-2018-04.html](https://bits.debian.org/2018/05/new-developers-2018-04.html)
