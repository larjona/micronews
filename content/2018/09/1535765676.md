Title: Debian Project News - 31 August 2018: Happy 25th Anniversary Debian, Debian 9.5 and 8.11 released, Alioth Migration to Salsa, Debian Data Protection Team, New features of Debian Package Tracker, Debian Policy 4.2.0.0 released, Reports, Calls for Help, and more! https://www.debian.org/News/weekly/2018/03/
Slug: 1535765676
Date: 2018-09-01 01:34
Author: Donald Norwood
Status: published

Debian Project News - 31 August 2018: Happy 25th Anniversary Debian, Debian 9.5 and 8.11 released, Alioth Migration to Salsa, Debian Data Protection Team, New features of Debian Package Tracker, Debian Policy 4.2.0.0 released, Reports, Calls for Help, and more! [https://www.debian.org/News/weekly/2018/03/](https://www.debian.org/News/weekly/2018/03/)
