Title: Transition news: GCC 6 enabled by default in unstable https://lists.debian.org/debian-devel-announce/2016/08/msg00001.html
Slug: 1470439546
Date: 2016-08-05 23:25
Author: Ana Guerrero López
Status: published

Transition news: GCC 6 enabled by default in unstable [https://lists.debian.org/debian-devel-announce/2016/08/msg00001.html](https://lists.debian.org/debian-devel-announce/2016/08/msg00001.html)
