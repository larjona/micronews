Title: There will be a mini-DebConf in Cambridge, UK in a few weeks. https://wiki.debian.org/DebianEvents/gb/2016/MiniDebConfCambridge
Slug: 1477144464
Date: 2016-10-22 13:54
Author: Ana Guerrero Lopez
Status: published

There will be a mini-DebConf in Cambridge, UK in a few weeks. [https://wiki.debian.org/DebianEvents/gb/2016/MiniDebConfCambridge](https://wiki.debian.org/DebianEvents/gb/2016/MiniDebConfCambridge)
