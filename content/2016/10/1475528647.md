Title: Several donations boost reliability of Debian's core infrastructure https://www.debian.org/News/2016/20161003
Slug: 1475528647
Date: 2016-10-03 21:04
Author: Laura Arjona Reina
Status: published

Several donations boost reliability of Debian's core infrastructure [https://www.debian.org/News/2016/20161003](https://www.debian.org/News/2016/20161003)
