Title: Norbert Tretkowski writes about Gajim plugins packaged for Debian https://tretkowski.de/2016/10/09/gajim-plugins-packaged-for-debian/
Slug: 1476114976
Date: 2016-10-10 15:56
Author: Laura Arjona Reina
Status: published

Norbert Tretkowski writes about Gajim plugins packaged for Debian [https://tretkowski.de/2016/10/09/gajim-plugins-packaged-for-debian/](https://tretkowski.de/2016/10/09/gajim-plugins-packaged-for-debian/)
