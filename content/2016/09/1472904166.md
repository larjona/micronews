Title: New DDs and DMs in July and August 2016. Congratulations! https://bits.debian.org/2016/09/new-developers-2016-08.html
Slug: 1472904166
Date: 2016-09-03 12:02
Author: Ana Guerrero López
Status: published

New DDs and DMs in July and August 2016. Congratulations! [https://bits.debian.org/2016/09/new-developers-2016-08.html](https://bits.debian.org/2016/09/new-developers-2016-08.html)
