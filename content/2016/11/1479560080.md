Title: A Debian Video Team Sprint is happening this weekend in Paris https://wiki.debian.org/Sprints/2016/DebConfVideoteamSprint20Nov2016
Slug: 1479560080
Date: 2016-11-19 12:54
Author: Laura Arjona Reina
Status: published

A Debian Video Team Sprint is happening this weekend in Paris [https://wiki.debian.org/Sprints/2016/DebConfVideoteamSprint20Nov2016](https://wiki.debian.org/Sprints/2016/DebConfVideoteamSprint20Nov2016)
