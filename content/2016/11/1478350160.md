Title: Call for proposals for the upcoming Mini Debian Conference Japan 2016 https://lists.debian.org/debian-events-apac/2016/11/msg00000.html
Slug: 1478350160
Date: 2016-11-05 12:49
Author: Ana Guerrero López
Status: published

Call for proposals for the upcoming Mini Debian Conference Japan 2016 [https://lists.debian.org/debian-events-apac/2016/11/msg00000.html](https://lists.debian.org/debian-events-apac/2016/11/msg00000.html)
