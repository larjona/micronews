Title: New mailing list created for porting Debian to free and open RISC-V CPU arch https://lists.debian.org/debian-riscv/
Slug: 1478084853
Date: 2016-11-02 11:07
Author: Paul Wise
Status: published

New mailing list created for porting Debian to free and open RISC-V CPU arch [https://lists.debian.org/debian-riscv/](https://lists.debian.org/debian-riscv/)
