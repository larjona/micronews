Title: Thanks Debian Video Team, the videos of the MiniDebConf Cambdrige 2016 are already available! http://ftp.acc.umu.se/pub/debian-meetings/2016/miniconf_cambridge16/
Slug: 1479117345
Date: 2016-11-14 09:55
Author: Laura Arjona Reina
Status: published

Thanks Debian Video Team, the videos of the MiniDebConf Cambdrige 2016 are already available! [http://meetings-archive.debian.net/pub/debian-meetings/2016/miniconf_cambridge16/](http://meetings-archive.debian.net/pub/debian-meetings/2016/miniconf_cambridge16/)
