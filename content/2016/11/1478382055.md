Title: Debian stretch has just entered its transition freeze https://lists.debian.org/debian-devel-announce/2016/11/msg00002.html
Slug: 1478382055
Date: 2016-11-05 21:40
Author: Niels Thykier
Status: published

Debian stretch has just entered its transition freeze [https://lists.debian.org/debian-devel-announce/2016/11/msg00002.html](https://lists.debian.org/debian-devel-announce/2016/11/msg00002.html)
