Title: Debian Project News - November 28th, 2016: News on Debian Stretch, Upcoming events, Event Reports, and more https://www.debian.org/News/weekly/2016/04/
Slug: 1480430191
Date: 2016-11-29 14:36
Author: Laura Arjona Reina
Status: published

Debian Project News - November 28th, 2016: News on Debian Stretch, Upcoming events, Event Reports, and more [https://www.debian.org/News/weekly/2016/04/](https://www.debian.org/News/weekly/2016/04/)
