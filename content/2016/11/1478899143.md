Title: Core Infrastructure Initiative Renews Funding for the Reproducible Builds Project. More Debian developers funded
Slug: 1478899143
Date: 2016-11-11 21:19
Author: Martin Zobel-Helas
Status: published

Core Infrastructure Initiative Renews Funding for the Reproducible Builds Project. More Debian developers funded [https://www.coreinfrastructure.org/news/announcements/2016/11/linux-foundations-core-infrastructure-initiative-renews-funding](https://www.coreinfrastructure.org/news/announcements/2016/11/linux-foundations-core-infrastructure-initiative-renews-funding)
