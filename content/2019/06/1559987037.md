Title: MiniDebConf Hamburg happening this weekend! All the details including link to the video streaming in  https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg
Slug: 1559987037
Date: 2019-06-08 09:43
Author: Laura Arjona Reina
Status: published

MiniDebConf Hamburg happening this weekend! All the details including link to the video streaming in  [https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg)
