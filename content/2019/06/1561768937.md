Title: Diversity and inclusion in Debian: small actions and large impacts https://bits.debian.org/2019/06/diversity-and-inclusion.html
Slug: 1561768937
Date: 2019-06-29 00:42
Author: Laura Arjona Reina
Status: published

Diversity and inclusion in Debian: small actions and large impacts [https://bits.debian.org/2019/06/diversity-and-inclusion.html](https://bits.debian.org/2019/06/diversity-and-inclusion.html)
