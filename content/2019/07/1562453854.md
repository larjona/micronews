Title: You don't need to be an expert to contribute to Debian! Assisting users with problems on the user support channels is a large contribution to Debian. https://www.debian.org/support #ReleasingDebianBuster
Slug: 1562453854
Date: 2019-07-06 22:57
Author: Donald Norwood
Status: published

You don't need to be an expert to contribute to Debian! Assisting users with problems on the user support channels is a large contribution to Debian. [https://www.debian.org/support](https://www.debian.org/support) #ReleasingDebianBuster
