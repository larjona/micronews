Title: Trying to upgrade to buster? Do you get this error 'N: This must be accepted explicitly before updates for this repository can be applied'? See https://bugs.debian.org/929248
Slug: 1562516297
Date: 2019-07-07 16:18
Author: Ana Guerrero Lopez
Status: published

Trying to upgrade to buster? Do you get this error 'N: This must be accepted explicitly before updates for this repository can be applied'? See [https://bugs.debian.org/929248](https://bugs.debian.org/929248)
