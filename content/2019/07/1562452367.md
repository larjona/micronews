Title: There are 34 vendors of Debian DVDs and USB sticks in 18 countries. Become a vendor today! https://www.debian.org/CD/vendors/ #ReleasingDebianBuster
Slug: 1562452367
Date: 2019-07-06 22:32
Author: Donald Norwood
Status: published

There are 34 vendors of Debian DVDs and USB sticks in 18 countries. Become a vendor today! [https://www.debian.org/CD/vendors/](https://www.debian.org/CD/vendors/) #ReleasingDebianBuster
