Title: CD testing is a manual process and has many combinations. If you want to help, you can join in at https://deb.li/3xcKC (coordination in #debian-cd on oftc) #releasingDebianBuster
Slug: 1562421277
Date: 2019-07-06 13:54
Author: Jonathan Wiltshire
Status: published

CD testing is a manual process and has many combinations. If you want to help, you can join in at [https://deb.li/3xcKC](https://deb.li/3xcKC) (coordination in #debian-cd on oftc) #releasingDebianBuster
