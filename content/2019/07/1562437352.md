Title: Be sure to check out the upgrading chapter of the Debian 10 buster release notes for your device, e.g. for 64-bits PC: https://www.debian.org/releases/buster/amd64/release-notes/ch-upgrading #releasingDebianBuster
Slug: 1562437352
Date: 2019-07-06 18:22
Author: Jonathan Wiltshire
Status: published

Be sure to check out the upgrading chapter of the Debian 10 buster release notes for your device, e.g. for 64-bits PC: [https://www.debian.org/releases/buster/amd64/release-notes/ch-upgrading](https://www.debian.org/releases/buster/amd64/release-notes/ch-upgrading) #releasingDebianBuster
