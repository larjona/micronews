Title: To celebrate the release of Debian Buster, PanikSchaf created a sock pattern featuring the Debian Swirl https://panikschaf.nopanik.org/posts/debian-swirl-socks/ Thanks!! #releasingDebianBuster
Slug: 1562422079
Date: 2019-07-06 14:07
Author: Laura Arjona Reina
Status: published

To celebrate the release of Debian Buster, PanikSchaf created a sock pattern featuring the Debian Swirl [https://panikschaf.nopanik.org/posts/debian-swirl-socks/](https://panikschaf.nopanik.org/posts/debian-swirl-socks/) Thanks!! #releasingDebianBuster
