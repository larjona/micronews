Title: Bits from the DPL (June 2019) https://lists.debian.org/debian-devel-announce/2019/07/msg00000.html
Slug: 1562165990
Date: 2019-07-03 14:59
Author: Laura Arjona Reina
Status: published

Bits from the DPL (June 2019) [https://lists.debian.org/debian-devel-announce/2019/07/msg00000.html](https://lists.debian.org/debian-devel-announce/2019/07/msg00000.html)
