Title: If any of the lines in your /etc/apt/sources.list refer to 'stable', you might get a surprise on your next upgrade! #ReleasingDebianBuster
Slug: 1562409601
Date: 2019-07-06 10:40
Author: Laura Arjona Reina and Alban Vidal
Status: published

If any of the lines in your /etc/apt/sources.list refer to 'stable', you might get a surprise on your next upgrade! #ReleasingDebianBuster
