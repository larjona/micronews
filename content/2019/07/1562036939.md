Title: Debian Project News - 1 July 2019: Debian 9.9 released, Release of Buster planned, New DPL, Events, Reports, More than just code, Calls for Help, RC bug report https://www.debian.org/News/weekly/2019/01/
Slug: 1562036939
Date: 2019-07-02 03:08
Author: Donald Norwood
Status: published

Debian Project News - 1 July 2019: Debian 9.9 released, Release of Buster planned, New DPL, Events, Reports, More than just code, Calls for Help, RC bug report [https://www.debian.org/News/weekly/2019/01/](https://www.debian.org/News/weekly/2019/01/)
