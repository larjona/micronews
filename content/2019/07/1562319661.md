Title: The #DebConf19 schedule is up, a few presentation slots still available https://lists.debian.org/debconf-announce/2019/07/msg00001.html
Slug: 1562319661
Date: 2019-07-05 09:41
Author: Laura Arjona Reina
Status: published

The #DebConf19 schedule is up, a few presentation slots still available [https://lists.debian.org/debconf-announce/2019/07/msg00001.html](https://lists.debian.org/debconf-announce/2019/07/msg00001.html)
