Title: There were 11 different themes proposed to be the official Debian buster theme. The Debian 10 buster desktop theme is futurePrototype https://wiki.debian.org/DebianArt/Themes/futurePrototype by Alex Makas. #ReleasingDebianBuster
Slug: 1562418180
Date: 2019-07-06 13:03
Author: Alban Vidal
Status: published

There were 11 different themes proposed to be the official Debian buster theme. The Debian 10 buster desktop theme is futurePrototype [https://wiki.debian.org/DebianArt/Themes/futurePrototype](https://wiki.debian.org/DebianArt/Themes/futurePrototype) by Alex Makas. #ReleasingDebianBuster
