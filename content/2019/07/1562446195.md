Title: There have been about 31 different official and unofficial ports to various kernels and hardware types https://www.debian.org/ports/ #ReleasingDebianBuster
Slug: 1562446195
Date: 2019-07-06 20:49
Author: Donald Norwood
Status: published

There have been about 31 different official and unofficial ports to various kernels and hardware types [https://www.debian.org/ports/](https://www.debian.org/ports/) #ReleasingDebianBuster
