Title: A new 'testing' suite has been created and populated ready for development of Debian 11 (code name 'bullseye') to begin #releasingDebianBuster
Slug: 1562417872
Date: 2019-07-06 12:57
Author: Jonathan Wiltshire
Status: published

A new 'testing' suite has been created and populated ready for development of Debian 11 (code name 'bullseye') to begin #releasingDebianBuster
