Title: In addition of the officially supported architectures, the Debian Ports Team maintains other architectures. See more info in https://www.ports.debian.org/
Slug: 1562455834
Date: 2019-07-06 23:30
Author: Donald Norwood
Status: published

In addition to the officially supported architectures, the Debian Ports Team maintains other architectures. See more info in [https://www.ports.debian.org/](https://www.ports.debian.org/)
