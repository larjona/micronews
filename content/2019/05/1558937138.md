Title: There will be a Debian booth in #PSES2019 near Paris from 27th to 30th June with #DebianFrance team. Come to greet us! https://passageenseine.fr 
Slug: 1558937138
Date: 2019-05-27 06:05
Author: Alban Vidal and Quentin Lejard
Status: published

There will be a Debian booth in #PSES2019 near Paris from 27th to 30th June with #DebianFrance team. Come to greet us! [https://passageenseine.fr](https://passageenseine.fr)
