Title: Mini-DebConf Marseille (France) is only in 10 days! See the schedules and register on https://minidebconf-mrs.debian.net #Debian #DebianFrance
Slug: 1557818390
Date: 2019-05-14 07:19
Author: Alban Vidal
Status: published

Mini-DebConf Marseille (France) is only in 10 days! See the schedules and register on [https://minidebconf-mrs.debian.net](https://minidebconf-mrs.debian.net) #Debian #DebianFrance
