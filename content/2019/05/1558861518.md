Title: MiniDebConf Marseille (France) happening this weekend! Follow the live streaming at https://minidebconf-mrs.debian.net/
Slug: 1558861518
Date: 2019-05-26 09:05
Author: Laura Arjona Reina
Status: published

MiniDebConf Marseille (France) happening this weekend! Follow the live streaming at [https://minidebconf-mrs.debian.net/](https://minidebconf-mrs.debian.net/)
