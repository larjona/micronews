Title: Congratulations to @Debian developer @simonmcvittie who has been shortlisted for a UK @opensourceaward! #UKOSA -- https://opensourceawards.org/the-shortlist/
Slug: 1558691074
Date: 2019-05-24 09:44
Author: Chris Lamb
Status: published

Congratulations to @Debian developer @simonmcvittie who has been shortlisted for a UK @opensourceaward! #UKOSA -- [https://opensourceawards.org/the-shortlist/](https://opensourceawards.org/the-shortlist/)
