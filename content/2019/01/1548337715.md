Title: This weekend we have a Debian Bug Squashing Party in Austin, Texas https://wiki.debian.org/BSP/2019/01/us/Austin #Debian #BSP #Party
Slug: 1548337715
Date: 2019-01-24 13:48
Author: Martin Zobel-Helas
Status: published

This weekend we have a Debian Bug Squashing Party in Austin, Texas [https://wiki.debian.org/BSP/2019/01/us/Austin](https://wiki.debian.org/BSP/2019/01/us/Austin) #Debian #BSP #Party
