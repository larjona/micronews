Title: Handshake donates $300,000 USD to Debian https://www.debian.org/News/2019/20190329
Slug: 1553896790
Date: 2019-03-29 21:59
Author: Laura Arjona Reina
Status: published

Handshake donates $300,000 USD to Debian [https://www.debian.org/News/2019/20190329](https://www.debian.org/News/2019/20190329)
