Title: Debian Project Leader Elections 2019: Call for nominations https://lists.debian.org/debian-devel-announce/2019/03/msg00000.html
Slug: 1551990143
Date: 2019-03-07 20:22
Author: Laura Arjona Reina
Status: published

Debian Project Leader Elections 2019: Call for nominations [https://lists.debian.org/debian-devel-announce/2019/03/msg00000.html](https://lists.debian.org/debian-devel-announce/2019/03/msg00000.html)
