Title: This weekend we have a #Debian Bug Squashing #Party in Berlin, Germany https://wiki.debian.org/BSP/2019/02/de/Berlin #BSP
Slug: 1549467545
Date: 2019-02-06 15:39
Author: Laura Arjona Reina
Status: published

This weekend we have a #Debian Bug Squashing #Party in Berlin, Germany [https://wiki.debian.org/BSP/2019/02/de/Berlin](https://wiki.debian.org/BSP/2019/02/de/Berlin) #BSP
