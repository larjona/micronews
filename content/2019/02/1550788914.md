Title: Debian, along with 30+ organizations, recently signed the Affirmation of the Open Source Definition. https://opensource.org/node/966 #opensource #freesoftware
Slug: 1550788914
Date: 2019-02-21 22:41
Author: Donald Norwood
Status: published

Debian, along with 30+ organizations, recently signed the Affirmation of the Open Source Definition. [https://opensource.org/node/966](https://opensource.org/node/966) #opensource #freesoftware
