Title: Projects and mentors for Debian's Google Summer of Code 2019 and Outreachy https://bits.debian.org/2019/02/project-and-mentors-gsoc-2019-outreachy.html
Slug: 1549190427
Date: 2019-02-03 10:40
Author: Laura Arjona Reina
Status: published

Projects and mentors for Debian's Google Summer of Code 2019 and Outreachy [https://bits.debian.org/2019/02/project-and-mentors-gsoc-2019-outreachy.html](https://bits.debian.org/2019/02/project-and-mentors-gsoc-2019-outreachy.html)
