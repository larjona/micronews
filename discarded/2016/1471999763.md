Title: Interested in participating in the first Debian Diversity IRC meeting? https://lists.debian.org/debian-devel-announce/2016/08/msg00012.html
Slug: 1471999763
Date: 2016-08-24 00:49
Author: Ana Guerrero López
Status: published

Interested in participating in the first Debian Diversity IRC meeting? [https://lists.debian.org/debian-devel-announce/2016/08/msg00012.html](https://lists.debian.org/debian-devel-announce/2016/08/msg00012.html)
